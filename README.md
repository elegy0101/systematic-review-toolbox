# Herramientas de ayuda para revisión sistematica 🔎🔧📕

El objetivo de este repositorio es compartir un set de herramientas de ayuda para la ejecución de la revisión sistemática. El contenido incluye un enlace al sitio oficial y una herramienta recomendada

## Sitio oficial ✅

**Systematic Review Tool Box** es un catálogo de herramientas basado en la nube, con capacidad de búsqueda y dirigido por la comunidad que apoya diversas tareas dentro del proceso de revisión sistemática.

El enlace al set de herramientas es el siguiente:

 [Systematic review tools](http://systematicreviewtools.com/)

## Parsifal ✅

**Parsifal** es una herramienta en línea diseñada para apoyar a los investigadores a realizar revisiones sistemáticas de la literatura dentro del contexto de la Ingeniería de Software.

Esta herramienta en particular es la que utilice para realizar la revisión sistemática de mi tesis ya que permite cubrir la mayoria de las fases del protocolo.

 [Parsifal](https://parsif.al/)

## Como contribuir

Este repositorio está bajo una licencia Creative Commons Attribution-ShareAlike 4.0. Todos son libres de agregar, editar y corregir.

Si alguien utiliza otra de las herramientas en particular que ofrece **SR Tool Box** siéntanse libres de comentarlo para agregarla al repo.

Si tienen alguna duda en la que pueda apoyar, con gusto.

¡Espero les sea de gran ayuda! 🤘